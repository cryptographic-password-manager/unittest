#!/usr/bin/env python3

import unittest
from example1 import exampleFunction


class StaticValueTest(unittest.TestCase):

    # test classes must begin with 'test'
    def testCorrect(self):
        self.assertEqual(exampleFunction(), int(1), "Function returns integer 1")

    def testFailing(self):
        self.assertEqual(exampleFunction(), "1", "Function returns string 1")


if __name__ == '__main__':
    unittest.main()
