# Readme


## Introduction
This repo is a basic introduction to unit testing.

## Requirements
This repo uses python3 with only built-in modules

## Usage
Each of these test classes can be run in your IDE of choice.

If you want to run these in the command line/bash, you can use ```python3 -m unittest exampleX/exampleXTest.py``` to run a specific class.

Otherwise you can use ```python3 -m unittest discover -p "*Test.py"``` to use test discovery to run all tests.

## Structure and Examples

### Example 1
Two simple tests to show what a passing and failing test look like

### Example 2
Two backfilled tests that pass and one that doesn't. You might wanna check the implementation to find out why backfilling tests isn't a good idea.

### Example 3
Example of a (decently) well written test class. Introduces error assertions, as well as testing complex functions using other implementatons

### Example 4
Oh no your production codebase has been deleted, luckily your test cases didn't get deleted as well. Use the test cases to figure out how the function should work.

There is a example answer should you get stuck

### Example 5
Example of how mocking can be used in order to simulate user input
