#!/usr/bin/env python3


def subtractFunction(x, y):
    if not isinstance(x, int) or not isinstance(y, int):
        print("Input needs to be an integer")
        raise TypeError()
    if y > x:
        return 0
    return x - y
