#!/usr/bin/env python3

import unittest
from example4 import subtractFunction as subtract
from operator import sub
from random import randint


class Example4Test(unittest.TestCase):

    def testNotInt(self):
        values = ["1", None, 2.035]
        for value in values:
            with self.subTest("test value = {}".format(value)):
                with self.assertRaises(TypeError):
                    subtract(value, value)

    def testSubtraction(self):
        x = randint(5, 10)
        y = randint(1, 4)
        expectedAnswer = sub(x, y)
        self.assertEqual(subtract(x, y), expectedAnswer, "{}+{}={}".format(x, y, expectedAnswer))

    def testNoNegatives(self):
        x = randint(0, 1)
        y = randint(2, 4)
        self.assertEqual(subtract(x, y), 0, "If y>x, returns 0")


if __name__ == '__main__':
    unittest.main()
