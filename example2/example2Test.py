#!/usr/bin/env python3

import unittest
from example2 import pluus


class PoorlyWrittenTest(unittest.TestCase):

    def testCase1(self):
        self.assertEqual(pluus(1, 1), 2)

    def testCase2(self):
        self.assertEqual(pluus(2, 2), 4)






    @unittest.skip("Failure Example")
    def testCase3(self):
        self.assertEqual(pluus(3, 3), 6, "3+3=6")
