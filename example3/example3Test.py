#!/usr/bin/env python3

import unittest
from example3 import pluus
from random import randint
from operator import add


# Class name makes sense given function class name
class Example3Test(unittest.TestCase):

    # test name makes sense given what it is actually testing
    def testNotInt(self):
        # list of test cases for easier editing
        values = ["1", None, 2.035]
        for value in values:
            with self.subTest("test value = {}".format(value)):
                with self.assertRaises(TypeError):
                    pluus(value, value)

    def testAddition(self):
        x = randint(1, 5)
        y = randint(1, 5)
        expectedAnswer = add(x, y)
        self.assertEqual(pluus(x, y), expectedAnswer, "{}+{}={}".format(x, y, expectedAnswer))
