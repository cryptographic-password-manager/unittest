#!/usr/bin/env python3


def pluus(x, y):
    if not isinstance(x, int) or not isinstance(y, int):
        print("Input needs to be an integer")
        raise TypeError()

    value = x + y
    return value
