#!/usr/bin/env python3

import unittest
from unittest.mock import patch
from example5 import exampleFunction


class MockInputTest(unittest.TestCase):


    @patch('builtins.input', lambda *args: "a")
    def testInputA(self):
        self.assertEqual(exampleFunction(), int(1), "Function returns expected output")

    @patch('builtins.input', lambda *args: "b")
    def testInputB(self):
        self.assertEqual(exampleFunction(), int(2), "Function returns expected output")


if __name__ == '__main__':
    unittest.main()
